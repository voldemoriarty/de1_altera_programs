# Rolling Message

This circuit displays a rolling message on the boards 4 seven segment LEDs. To set the message use the python script `prepmsg.py` like `python3 prepmsg.py "hello world"`.
Then resynthesize and burn.
