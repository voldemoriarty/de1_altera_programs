module msg_mem(
	input [9:0] addr,
	output reg [6:0] char
);
	reg [6:0] mem[1023:0];
	
	always @(addr) 
		char <= mem[addr];
	
	initial
		$readmemb("msg.txt", mem);
endmodule


module top(
	output reg [6:0] HEX3, HEX2, HEX1, HEX0,
	input CLOCK_50, input [0:0] KEY
);

// addr ctr
wire rst, clk;
reg [9:0] ctr;
wire [6:0] char;
reg [24:0] clk_div_ctr; 	// for approx 1Hz signal from 50MHz

assign rst = KEY[0];
assign clk = clk_div_ctr[23];

msg_mem rom(ctr, char);

// clock divider
always @(posedge CLOCK_50 or negedge rst) begin
	if (!rst) clk_div_ctr <= 'b0;
	else clk_div_ctr <= clk_div_ctr + 1'b1;
end


// register chain
always @(posedge clk or negedge rst) begin
	if (!rst) begin 
		{HEX3, HEX2, HEX1, HEX0} <= 28'hFFFFFFF;
	end
	else begin
		HEX3 <= HEX2;
		HEX2 <= HEX1;
		HEX1 <= HEX0;
		HEX0 <= char;
	end
end

// counter logic
always @(posedge clk or negedge rst) begin
	if (!rst) ctr <= 'b0;
	else ctr <= ctr + 1'b1;
end
	
endmodule
