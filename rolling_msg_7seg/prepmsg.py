#!/usr/bin/python3 
import sys

msg_len = 7
msg_num = 1 << 10

dummy = "0" * msg_len + "\n"
msg = ""

if len(sys.argv) == 1:
	print("No message to encode")
	exit(0)

if len(sys.argv[1]) > msg_num:
	print("Message too long. Max chars: " + str(msg_num))
	exit(1)

arg = sys.argv[1].lower()

lut = {
	"a":"3",
	"b":"01",
	"c":"1250",
	"d":"05",
	"e":"12",
	"f":"321",
	"g":"43",
	"h":"013",
	"i":"06312",
	"j":"5460",
	"k":"31",
	"l":"2610",
	"m":"51",
	"n":"3510",
	"o":"501",
	"p":"32",
	"q":"43",
	"r":"10523",
	"s":"14",
	"t":"012",
	"u":"06",
	"v":"1065",
	"w":"42",
	"x":"03",
	"y":"40",
	"z":"52",
	"0":"6",
	"1":"05643",
	"2":"52",
	"3":"54",
	"4":"430",
	"5":"14",
	"6":"1",
	"7":"5643",
	"8":"",
	"9":"4",
	" ":"0123456",
	"-":"105432"
}

count = 0
for char in arg:
	if char not in lut:
		char = "-"
	ones = lut[char]
	for i in [6,5,4,3,2,1,0]:
		if str(i) in ones: msg += "1"
		else: msg += "0"
	
	msg += "\n"

rep = int(msg_num // len(arg)) - 1
rem = int(msg_num % len(arg))
msg += msg * rep
msg += dummy * rem

open("msg.txt", "w").write(msg)
